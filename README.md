# The Fediverse

The Fediverse is a social internet, owned and operated by people. It is made of protocols implemented in projects that let users create instances that connect to one another. The Fediverse is the marriage of Free Open Source Software, the internet, and the people.

The Fediverse however, is not perfect. There are many problems with it, just as with every area of human endeavor.

Before we can solve those problems, we need to determine what they are, seek consensus on their accuracy, and prove the value of solving them. In this process, we may draw the attention of those who can develop solutions.

# Language
The key words “MUST”, “MUST NOT”, “REQUIRED”, “SHALL”, “SHALL NOT”, “SHOULD”, “SHOULD NOT”, “RECOMMENDED”, “MAY”, and “OPTIONAL” in this document are to be interpreted as described in [RFC 2119](https://datatracker.ietf.org/doc/html/rfc2119).

# Process
1. The user or Contributor SHOULD search for an issue that describes the problem they face or observe.
1. If none is found, the user or Contributor SHOULD write the issue by describing the problem they face or observe.
1. The user or Contributor SHOULD seek consensus on the accuracy of their observation, and the value of solving the problem.
1. Users SHALL NOT log feature requests, ideas, suggestions, or any solutions to problems that are not explicitly documented and provable.
